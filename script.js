const work_button = document.getElementById("work_button");
const bank_button = document.getElementById("bank_button");
const loan_button = document.getElementById("loan_button");
const repay_button = document.getElementById("repay_button");
const pay_el = document.getElementById("pay");
const balance_el = document.getElementById("balance");
const outstanding_loan_el = document.getElementById("outstanding_loan");
const loan_info_el = document.getElementById("loan_info");
const products_el = document.getElementById("laptops_in_menu");
const laptop_features = document.getElementById("featuresz");
const laptop_desc = document.getElementById("laptop_desc");
const buy_now_button = document.getElementById("buy_now_button");
const laptop_image = document.getElementById("laptop_image");
const laptop_price = document.getElementById("price");
const display_laptops = document.getElementById("display_laptops");

let pay = 0;
let balance = 0;
let outstanding_loan = 0;
let products = [];

loan_info_el.hidden = true;
repay_button.hidden = true;
display_laptops.hidden = true;

// When the "Repay loan" button is clicked the current pay is used to repay loan instead of
// being added to balance
repay_button.addEventListener("click", () => {
  if (pay > outstanding_loan) {
    pay = pay - outstanding_loan;
    outstanding_loan = 0;
    balance += pay;
  } else {
    outstanding_loan -= pay;
  }
  if (outstanding_loan === 0) {
    repay_button.hidden = true;
    loan_info_el.hidden = true;
  }
  pay = 0;
  outstanding_loan_el.innerText = outstanding_loan;
  balance_el.innerText = balance;
  pay_el.innerText = pay;
});

// When the "Work" button is clicked money is added to pay
work_button.addEventListener("click", () => {
  pay += 100;
  pay_el.innerText = pay;
});

// When the "Bank" button is clicked the money stored in pay will be transfered to balance
// If the user has an outstanding loan 10% will be used to repay the loan instead
bank_button.addEventListener("click", () => {
  if (outstanding_loan != 0) {
    if (outstanding_loan < pay * 0.1) {
      pay = pay - outstanding_loan;
      outstanding_loan = 0;
    } else {
      outstanding_loan -= pay * 0.1;
      pay = pay * 0.9;
    }
    outstanding_loan_el.innerText = outstanding_loan;
  }
  if (outstanding_loan === 0) {
    repay_button.hidden = true;
    loan_info_el.hidden = true;
  }
  balance += pay;
  balance_el.innerText = balance;
  pay = 0;
  pay_el.innerText = pay;
});

// When the "Loan" button is clicked the user is asked to input an amount
// If the amount is valid the user gets a loan and the loan amount and repay button becomes visable
loan_button.addEventListener("click", () => {
  if (outstanding_loan === 0) {
    let amount = Number(prompt("Please enter an amount:"));
    if (isNaN(amount) || amount < 0) {
      alert("Please input a number between 0 kr and " + balance * 0.5 + "kr.");
      return 0;
    }
    if (amount > balance * 2) {
      alert("Your bank balance is too low, please try a lower amount");
      return 0;
    } else {
      balance += amount;
      balance_el.innerText = balance;
      outstanding_loan = amount;
      outstanding_loan_el.innerText = outstanding_loan;
      if (outstanding_loan != 0) {
        repay_button.hidden = false;
        loan_info_el.hidden = false;
      }
    }
  } else {
    window.alert("Sorry, you already have a loan");
  }
});

// If the user clicks the "Buy now" button, and the user can afford the laptop, money is
// taken from the users' balance
buy_now_button.addEventListener("click", () => {
  if (balance < Number(laptop_price.innerText)) {
    window.alert("You need to work harder");
  } else {
    balance -= Number(laptop_price.innerText);
    balance_el.innerText = balance;
    window.alert("Congratulation!");
  }
});

// The API is fetched and stored in the variable "products"
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => response.json())
  .then((data) => (products = data))
  .then((products) => add_products_menu(products));

const add_products_menu = (products) => {
  products.forEach((x) => add_product(x));
};

// The laptops from the API are added as options for the select box
const add_product = (product) => {
  const product_el = document.createElement("option");
  product_el.value = product.id;
  product_el.appendChild(document.createTextNode(product.title));
  products_el.appendChild(product_el);
};

// When an option is chosen in the select box the specifications, description, price, and image of
// the corresponding laptop is displayed
products_el.addEventListener("change", (event) => {
  display_laptops.hidden = false;
  const target = event.target;
  laptop_features.innerText = products[target.value - 1].specs.join("\n");
  laptop_desc.innerText = products[target.value - 1].description;
  laptop_price.innerText = products[target.value - 1].price;
  laptop_image.src =
    "https://noroff-komputer-store-api.herokuapp.com/" +
    products[target.value - 1].image;
});
